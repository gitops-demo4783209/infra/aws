# Terraform for Kubernetes Cluster on AWS

## GitOps Demo Group
See [Global Readme file](https://gitlab.com/gitops-demo/readme/-/blob/master/README.md) for the full details.

```
├── backend.tf              # State file Location Configuration
├── eks.tf                  # Amazon EKS Configuration
├── gitlab-gitops-agent.tf  # Adding GitLab kubernetes agent to cluster.
└── vpc.tf                  # AWS VPC Configuration
```
